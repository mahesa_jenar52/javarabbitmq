/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Listenner;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;
import servlet.ServletRe;
import thread.MyThread;

/**
 *
 * @author Geo
 */
public class MyListenner implements ServletContextListener{
    
    private static MyThread mythread = null;
    private ServletRe re = new ServletRe();
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        if(mythread==null || !mythread.isAlive()){
            
            mythread = new MyThread();
            mythread.setMyServlet(re);
            mythread.setDaemon(true);
            mythread.start();
            
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try{
            mythread.killThread();
        }catch(Exception e){
            
        }
    }
    public static MyThread getMyThread(){
        return mythread;
    }
    
}
