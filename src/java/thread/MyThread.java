/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package thread;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import servlet.ServletRe;

/**
 *
 * @author Geo
 */
public class MyThread extends Thread{
    
    private boolean isRunning = true;
    private static final String QUEUE_NAME = "test";
    private QueueingConsumer consumer = null;
    private static String message = "";
    private ServletRe serv;
    public MyThread(){
        
    }
    
    public void run(){
       
            
        
        while(isRunning){
            try {            
                ConnectionFactory factory = new ConnectionFactory();
                factory.setHost("localhost");
                Connection connection = factory.newConnection();
                Channel channel = connection.createChannel();
                channel.queueDeclare(QUEUE_NAME,false,false,false,null);
                consumer = new QueueingConsumer(channel);

                channel.basicConsume(QUEUE_NAME, true,consumer);
                
                while(true){
                     QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                     message = new String(delivery.getBody());
                     serv.setMessage(message);
                     Thread.sleep(10);
                }
                
            } catch (IOException ex) {
                Logger.getLogger(MyThread.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(MyThread.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        }
         
    }
    public void killThread(){
        isRunning = false;
    }
    
    public void setMyServlet(ServletRe serv){
        this.serv = serv;
    }
    
    public String getMessage(){
        return message;
    }
}
